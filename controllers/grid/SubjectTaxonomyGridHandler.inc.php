<?php

/**
 * @file controllers/grid/SubjectTaxonomyGridHandler.inc.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @class SubjectTaxonomyGridHandler
 * @ingroup controllers_grid_subjectTaxonomy
 *
 * @brief Handle Subject Taxonmoy grid requests.
 */

import('lib.pkp.classes.controllers.grid.GridHandler');
import('plugins.generic.subjectTaxonomy.controllers.grid.SubjectTaxonomyGridRow');
import('plugins.generic.subjectTaxonomy.controllers.grid.SubjectTaxonomyGridCellProvider');
import('classes.notification.NotificationManager');

class SubjectTaxonomyGridHandler extends GridHandler {
	/** @var SubjectTaxonomyPlugin The Subject Taxonmoy plugin */
	static $plugin;

	/**
	 * Set the Subject Taxonmoy plugin.
	 * @param $plugin SubjectTaxonomyPlugin
	 */
	static function setPlugin($plugin) {
		self::$plugin = $plugin;
	}

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		$this->addRoleAssignment(
			array(ROLE_ID_MANAGER),
			array('index', 'fetchGrid', 'fetchRow', 'addSubjectTaxonomy', 'editSubjectTaxonomy', 'updateSubjectTaxonomy', 'delete', 'saveSequence')
		);
	}


	//
	// Overridden template methods
	//
	/**
	 * @copydoc PKPHandler::authorize()
	 */
	function authorize($request, &$args, $roleAssignments) {
		import('lib.pkp.classes.security.authorization.ContextAccessPolicy');
		$this->addPolicy(new ContextAccessPolicy($request, $roleAssignments));
		return parent::authorize($request, $args, $roleAssignments);
	}

	/**
	 * @copydoc GridHandler::initialize()
	 */
	function initialize($request, $args = null) {
		parent::initialize($request, $args);
		$context = $request->getContext();

		// Set the grid details.
		$this->setTitle('plugins.generic.subjectTaxonomy.subjectTaxonomy');
		$this->setEmptyRowText('plugins.generic.subjectTaxonomy.noneCreated');

		// Get the pages and add the data to the grid
		$subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
		$subjectTaxonomyIterator = $subjectTaxonomyDao->getByContextId($context->getId());
		
		$gridData = array();
		while ($subjectTaxonomy = $subjectTaxonomyIterator->next()) {
			$subjectTaxonomyId = $subjectTaxonomy->getId();
			$gridData[$subjectTaxonomyId] = array(
				'title' => $subjectTaxonomy->getLocalizedTitle(),
				'seq' => $subjectTaxonomy->getSequence()
			);
		}
		uasort($gridData, function($a,$b) {
			return $a['seq']-$b['seq'];
		});
		$this->setGridDataElements($gridData);

		// Add grid-level actions
		$router = $request->getRouter();
		import('lib.pkp.classes.linkAction.request.AjaxModal');
		$this->addAction(
			new LinkAction(
				'addSubjectTaxonomy',
				new AjaxModal(
					$router->url($request, null, null, 'addSubjectTaxonomy'),
					__('plugins.generic.subjectTaxonomy.addSubjectTaxonomy'),
					'modal_add_item'
				),
				__('plugins.generic.subjectTaxonomy.addSubjectTaxonomy'),
				'add_item'
			)
		);

		// Columns
		$cellProvider = new SubjectTaxonomyGridCellProvider();
		$this->addColumn(new GridColumn(
			'title',
			'plugins.generic.subjectTaxonomy.title',
			null,
			'controllers/grid/gridCell.tpl', // Default null not supported in OMP 1.1
			$cellProvider
		));
	}

	//
	// Overridden methods from GridHandler
	//
	/**
	 * @copydoc GridHandler::getRowInstance()
	 */
	function getRowInstance() {
		return new SubjectTaxonomyGridRow();
	}

	//
	// Overridden methods from GridHandler
	//
	/**
	 * @copydoc GridHandler::initFeatures()
	 */
	function initFeatures($request, $args) {
		import('lib.pkp.classes.controllers.grid.feature.OrderGridItemsFeature');
		return array(new OrderGridItemsFeature());
	}

	/**
	 * @copydoc GridHandler::getDataElementSequence()
	 */
	function getDataElementSequence($row) {
		return $row['seq'];
	}

	/**
	 * @copydoc GridHandler::setDataElementSequence()
	 */
	function setDataElementSequence($request, $rowId, $gridDataElement, $newSequence) {
		$subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
		$context = $request->getContext();
		$subjectTaxonomy = $subjectTaxonomyDao->getById($rowId, $context->getId());
		$subjectTaxonomy->setSequence($newSequence);
		$subjectTaxonomyDao->updateObject($subjectTaxonomy);
	}

	//
	// Public Grid Actions
	//
	/**
	 * Display the grid's containing page.
	 * @param $args array
	 * @param $request PKPRequest
	 */
	function index($args, $request) {
		$context = $request->getContext();
		import('lib.pkp.classes.form.Form');
		$form = new Form(self::$plugin->getTemplateResource('subjectTaxonomy.tpl'));
		return new JSONMessage(true, $form->fetch($request));
	}

	/**
	 * An action to add a new custom subject
	 * @param $args array Arguments to the request
	 * @param $request PKPRequest Request object
	 */
	function addSubjectTaxonomy($args, $request) {
		// Calling editSubjectTaxonomy with an empty ID will add
		// a new subject.
		return $this->editSubjectTaxonomy($args, $request);
	}

	/**
	 * An action to edit a subject
	 * @param $args array Arguments to the request
	 * @param $request PKPRequest Request object
	 * @return string Serialized JSON object
	 */
	function editSubjectTaxonomy($args, $request) {
		$subjectTaxonomyId = $request->getUserVar('subjectTaxonomyId');
		$context = $request->getContext();
		$this->setupTemplate($request);

		// Create and present the edit form
		import('plugins.generic.subjectTaxonomy.controllers.grid.form.SubjectTaxonomyForm');
		$subjectTaxonomyPlugin = self::$plugin;
		$subjectTaxonomyForm = new SubjectTaxonomyForm(self::$plugin, $context->getId(), $subjectTaxonomyId);
		$subjectTaxonomyForm->initData();
		return new JSONMessage(true, $subjectTaxonomyForm->fetch($request));
	}

	/**
	 * Update a custom block
	 * @param $args array
	 * @param $request PKPRequest
	 * @return string Serialized JSON object
	 */
	function updateSubjectTaxonomy($args, $request) {
		$subjectTaxonomyId = $request->getUserVar('subjectTaxonomyId');
		$context = $request->getContext();
		$this->setupTemplate($request);

		// Create and populate the form
		import('plugins.generic.subjectTaxonomy.controllers.grid.form.SubjectTaxonomyForm');
		$subjectTaxonomyPlugin = self::$plugin;
		$subjectTaxonomyForm = new SubjectTaxonomyForm(self::$plugin, $context->getId(), $subjectTaxonomyId);
		$subjectTaxonomyForm->readInputData();

		$notificationMgr = new NotificationManager();
		$user = $request->getUser();

		// Check the results
		if ($subjectTaxonomyForm->validate()) {
			// Save the results
			$subjectTaxonomyForm->execute();

			$notificationMgr->createTrivialNotification($user->getId(), NOTIFICATION_TYPE_SUCCESS, array('contents' => __('plugins.generic.subjectTaxonomy.'.(($subjectTaxonomyId)?'updated':'added'))));

 			return DAO::getDataChangedEvent();
		} else {
			$notificationMgr->createTrivialNotification($user->getId(), NOTIFICATION_TYPE_ERROR, array('contents' => __('plugins.generic.subjectTaxonomy.notUpdated')));
			// Present any errors
			return new JSONMessage(true, $subjectTaxonomyForm->fetch($request));
		}
	}

	/**
	 * Delete a subject
	 * @param $args array
	 * @param $request PKPRequest
	 * @return string Serialized JSON object
	 */
	function delete($args, $request) {
		$subjectTaxonomyId = $request->getUserVar('subjectTaxonomyId');
		$context = $request->getContext();

		// Delete the subject
		$subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
		$subjectTaxonomy = $subjectTaxonomyDao->getById($subjectTaxonomyId, $context->getId());
		$subjectTaxonomyDao->deleteObject($subjectTaxonomy);

		$notificationMgr = new NotificationManager();
		$user = $request->getUser();
		$notificationMgr->createTrivialNotification($user->getId(), NOTIFICATION_TYPE_SUCCESS, array('contents' => __('plugins.generic.subjectTaxonomy.deleted')));

		return DAO::getDataChangedEvent();
	}
}

