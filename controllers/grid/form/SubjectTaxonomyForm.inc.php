<?php

/**
 * @file controllers/grid/form/SubjectTaxonomyForm.inc.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @class SubjectTaxonomyForm
 * @ingroup controllers_grid_subjectTaxonomy
 *
 * Form for press managers to create and modify subjects
 *
 */

import('lib.pkp.classes.form.Form');

class SubjectTaxonomyForm extends Form {
	/** @var int Context (press / journal) ID */
	var $contextId;

	/** @var Subject */
	var $subjectTaxonomy;

	/** @var SubjectTaxonomyPlugin Subject Taxonmoy plugin */
	var $plugin;

	/**
	 * Constructor
	 * @param $subjectTaxonomyPlugin SubjectTaxonomyPlugin The subject plugin
	 * @param $contextId int Context ID
	 * @param $subjectTaxonomyId int subject ID (if any)
	 */
	function __construct($subjectTaxonomyPlugin, $contextId, $subjectTaxonomyId = null) {
		$subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
		parent::__construct($subjectTaxonomyPlugin->getTemplateResource('editSubjectTaxonomyForm.tpl'));
		
		$this->contextId = $contextId;
		$this->plugin = $subjectTaxonomyPlugin;
		$this->subjectTaxonomy = $subjectTaxonomyDao->getById($subjectTaxonomyId, $this->contextId);

		// Add form checks
		$this->addCheck(new FormValidatorPost($this));
		$this->addCheck(new FormValidatorCSRF($this));
		$this->addCheck(new FormValidator($this, 'title', 'required', 'plugins.generic.subjectTaxonomy.nameRequired'));
	}

	/**
	 * Initialize form data from current group group.
	 */
	function initData() {
		if (isset($this->subjectTaxonomy)) {
			$locale = AppLocale::getLocale();
			$this->_data = array(
				'title' => $this->subjectTaxonomy->getTitle(null), // Localized
			);
			parent::initData();
		}	}

	/**
	 * Assign form data to user-submitted data.
	 */
	function readInputData() {
		$this->readUserVars(array('title'));
	}

	/**
	 * @copydoc Form::fetch
	 */
	function fetch($request, $template = null, $display = false) {
		$templateMgr = TemplateManager::getManager();
		$templateMgr->assign(array(
			'pluginJavaScriptURL' => $this->plugin->getJavaScriptURL($request),
		));
		if ($this->subjectTaxonomy) {
			$templateMgr->assign(array(
				'subjectTaxonomy' => $this->subjectTaxonomy,
				'subjectTaxonomyId' => $this->subjectTaxonomy->getId(),
			));
		}

		return parent::fetch($request, $template, $display);
	}

	/**
	 * Get a list of fields for which localization should be used.
	 * @return array
	 */
	function getLocaleFieldNames() {
		$subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO'); /* @var $issueDao IssueDAO */
		return $subjectTaxonomyDao->getLocaleFieldNames();
	}

	/**
	 * Save form values into the database
	 */
	function execute(...$functionParams) {
		parent::execute(...$functionParams);

		$subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
		if ($this->subjectTaxonomy) {
			// Load and update an existing subject
			$isNewSubject = false;
			$subjectTaxonomy = $this->subjectTaxonomy;
		} else {
			// Create a new subject
			$subjectTaxonomy = $subjectTaxonomyDao->newDataObject();
			$isNewSubject = true;
		}

		$subjectTaxonomy->setTitle($this->getData('title'), null); // Localized

		if ($isNewSubject) {
			$subjectTaxonomy->setContextId($this->contextId);
			$subjectTaxonomy->setSequence(REALLY_BIG_NUMBER);
			$subjectTaxonomyDao->insertObject($subjectTaxonomy);
		} else {
			$locale = AppLocale::getLocale();
			$subjectTaxonomyDao->updateObject($subjectTaxonomy);
		}
	}
}

