# OJS Subject Taxonomy Plugin 

Gives the ability to add subjects and assign them through drop down list instead of a text box.

## OJS compatibility

* OJS `3.3.*`: please install version version `0.3.*`+
* OJS `3.2.1-*`+: please install version `0.2.*`+
