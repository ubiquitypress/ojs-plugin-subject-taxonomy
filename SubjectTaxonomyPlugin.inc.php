<?php

/**
 * @file SubjectTaxonomyPlugin.inc.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @package plugins.generic.subjectTaxonomy
 * @class SubjectTaxonomyPlugin
 * Subject Taxonmoy plugin main class
 */

import('lib.pkp.classes.plugins.GenericPlugin');
use \PKP\components\forms\FieldSelect;
class SubjectTaxonomyPlugin extends GenericPlugin {

	/**
	 * @copydoc Plugin::register()
	 */
	function register($category, $path, $mainContextId = null) {
		if (parent::register($category, $path, $mainContextId)) {
			if ($this->getEnabled($mainContextId)) {
                HookRegistry::register('TemplateResource::getFilename', array($this, 'overridePluginTemplates'));
                // Register the Subject Taxonmoy DAO.
                import('plugins.generic.subjectTaxonomy.classes.SubjectTaxonomyDAO');
                $subjectTaxonomyDao = new SubjectTaxonomyDAO();
                DAORegistry::registerDAO('SubjectTaxonomyDAO', $subjectTaxonomyDao);

                // Register the components this plugin implements to
                // permit administration of Subject Taxonmoy.
                HookRegistry::register('LoadComponentHandler', array($this, 'setupGridHandler'));

                // Add Subject Texonomy field to Submission form step 3 and publication metadata form
                // Hooks for Submission form step 3
                HookRegistry::register('Templates::Submission::SubmissionMetadataForm::AdditionalMetadata', array($this, 'metadataFieldEdit'));

                // Hooks to extend quick submit plugin form
                HookRegistry::register('quicksubmitform::display', array($this, 'metadataInitData'));
                HookRegistry::register('quicksubmitform::readuservars', array($this, 'metadataReadUserVars'));
                HookRegistry::register('quicksubmitform::execute', array($this, 'quickSubmitExecute'));

                HookRegistry::register('submissionsubmitstep3form::initdata', array($this, 'metadataInitData'));
                HookRegistry::register('submissionsubmitstep3form::readuservars', array($this, 'metadataReadUserVars'));
                HookRegistry::register('submissionsubmitstep3form::execute', array($this, 'metadataExecute'));

                // Hooks for Submission form Publication tab
                HookRegistry::register('Schema::get::publication', array($this, 'addToSchema'));
                HookRegistry::register('Form::config::before', array($this, 'addToForm'));
			}
			return true;
		}
		return false;
	}

	/*************************/
    /**** Plugin settings ****/
    /*************************/
    
    /**
    * @copydoc Plugin::isSitePlugin()
    */
    function isSitePlugin() {
        // This is a site-wide plugin.
        return true;
    }

	/**
	 * @copydoc Plugin::getDisplayName()
	 */
	function getDisplayName() {
		return __('plugins.generic.subjectTaxonomy.displayName');
	}

	/**
	 * @copydoc Plugin::getDescription()
	 */
	function getDescription() {
		$description = __('plugins.generic.subjectTaxonomy.description');
		return $description;
	}

    /**
     * @copydoc Plugin::getInstallMigration()
     */
    function getInstallMigration() {
        $this->import('SubjectTaxonomySchemaMigration');
        return new SubjectTaxonomySchemaMigration();
    }
    
    /**
     * @copydoc Plugin::getInstallSitePluginSettingsFile()
     * get the plugin settings
     */
    function getInstallSitePluginSettingsFile() {
        return $this->getPluginPath() . '/settings.xml';
    }

    /**
     * override ojs templates
     */
    function overridePluginTemplates($hookName, $args) {
        Parent::_overridePluginTemplates($hookName, $args);
        $request = PKPApplication::get()->getRequest();
        $templateMgr = TemplateManager::getManager();
        $templateMgr->assign(array(
            'subjecttaxonomyEnabled' => true,
        ));
    }

	/***************************************/
    /********** Journal tab content*********/
    /***************************************/
	/**
	 * Get the JavaScript URL for this plugin.
	 */
	function getJavaScriptURL($request) {
		return $request->getBaseUrl() . '/' . $this->getPluginPath() . '/js';
	}

	/**
	 * Permit requests to the Subject Taxonmoy grid handler
	 * @param $hookName string The name of the hook being invoked
	 * @param $params array The parameters to the invoked hook
	 */
	function setupGridHandler($hookName, $params) {
		$component =& $params[0];
		if ($component == 'plugins.generic.subjectTaxonomy.controllers.grid.SubjectTaxonomyGridHandler') {
			// Allow the static page grid handler to get the plugin object
			import($component);
			SubjectTaxonomyGridHandler::setPlugin($this);
			return true;
		}
		return false;
	}

	/**
	 * @copydoc Plugin::getActions()
	 */
	function getActions($request, $actionArgs) {
		$dispatcher = $request->getDispatcher();
		import('lib.pkp.classes.linkAction.request.RedirectAction');
		return array_merge(
			$this->getEnabled()?array(
				new LinkAction(
					'settings',
					new RedirectAction($dispatcher->url(
						$request, ROUTE_PAGE,
						null, 'management', 'settings', 'context',
						array('uid' => uniqid()), // Force reload
						'subjectTaxonomy-button' // Anchor for tab
					)),
					__('plugins.generic.subjectTaxonomy.editAddContent'),
					null
				),
			):array(),
			parent::getActions($request, $actionArgs)
		);
	}

	/******************************/
    /**** Publication Metadata ****/
    /******************************/

    /**
     * Extend the publication entity's schema with an subjectTaxonomy property
     */
    public function addToSchema($hookName, $args)
    {
        $schema = $args[0];
        $schema->properties->subjectTaxonomy = (object)[
            'type' => 'integer',
            'validation' => ['nullable'],
        ]; 
    }

    /**
     * Extend the Publication form to add an subjectTaxonomy input field
     *
     * @param $hookName string
     * @param $form array
     *
     */
    public function addtoForm($hookName, $form)
    {
        // Only modify the metadata form
        if (!defined('FORM_METADATA') || $form->id !== FORM_METADATA) {
            return;
        }
		
		$request = PKPApplication::get()->getRequest();

		// get the current context
		$context = $request->getContext();
		// Get all subjects
		$subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
		$subjects = $subjectTaxonomyDao->getByContextId($context->getId());
		$options = [0 => ''];
		while ($subjectTaxonomy = $subjects->next()) {
			$subjectTaxonomyId = $subjectTaxonomy->getId();
			$subjectTaxonomyTitle = $subjectTaxonomy->getLocalizedTitle();
			$options[] = ['value' => $subjectTaxonomyId, 'label' => $subjectTaxonomyTitle];
		}
        
        $form->removeField('subjects');
        // Add a field to the form
        if (count($options) > 1) {
            $form->addField(new FieldSelect('subjectTaxonomy', [
                'label' => __('plugins.generic.subjectTaxonomy.subjectTaxonomy'),
                'options' => $options,
            ]), array(FIELD_POSITION_BEFORE, 'keywords'));
        }
    }

    /*******************************/
    /***** Submission Metadata *****/
    /*******************************/

    /**
     * Insert Subject Taxonomy field into meta data submission step 3 and metadata edit form
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataFieldEdit($hookName, $params)
    {
        // get the template info
        $smarty =& $params[1];
        $output =& $params[2];
        // add the new Subject Taxonomy field block to the template form
        $output .= $smarty->fetch($this->getTemplateResource('subjectTaxonomy.tpl'));
        return false;
    }

    /**
     * Init Subject Taxonomy value
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataInitData($hookName, $params)
    {
        // get the current form values
        $form =& $params[0];

        if (get_class($form) == 'SubmissionSubmitStep3Form') {
			$request = PKPApplication::get()->getRequest();
            $templateMgr = TemplateManager::getManager($request);

            // get the current context
			$context = $request->getContext();
            $context->setData('subjects', 0);
			// Get all subjects
            $subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
            $subjects = $subjectTaxonomyDao->getByContextId($context->getId());
            $options = [0 => ''];
			while ($subjectTaxonomy = $subjects->next()) {
				$subjectTaxonomyId = $subjectTaxonomy->getId();
				$options[$subjectTaxonomyId] = $subjectTaxonomy->getLocalizedTitle();
			}
        	
        	// Get current publication
        	$publication = $form->submission->getCurrentPublication();
            $pubId = $publication->getData('id');
            // Get current subject
            $subjectTaxonomy = $publication->getData('subjectTaxonomy');
            // add the value to the form
            $publication->setData('subjectTaxonomy', $subjectTaxonomy);
            // add the current data to the template
            $templateVars = array('options' => $options, 'value' => $subjectTaxonomy);
            // get the template
            $templateMgr->assign($templateVars);
        } elseif (get_class($form) == 'QuickSubmitForm') {
            $request = PKPApplication::get()->getRequest();
            $templateMgr = TemplateManager::getManager($request);

            // get the current context
            $context = $request->getContext();
            // Get all subjects
            $subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
            $subjects = $subjectTaxonomyDao->getByContextId($context->getId());
            $options = [0 => ''];
            while ($subjectTaxonomy = $subjects->next()) {
                $subjectTaxonomyId = $subjectTaxonomy->getId();
                $options[$subjectTaxonomyId] = $subjectTaxonomy->getLocalizedTitle();
            }

            // add the current data to the template
            $templateVars = array('options' => $options);
            // get the template

            $templateMgr->assign($templateVars);
        }
    }

    /**
     * Init Subject Taxonomy value
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function quickSubmitExecute($hookName, $params)
    {
        // submitting the form with the new Competing Interests value
        // via quickSubmit plugin
        $request = PKPApplication::get()->getRequest();
        $form = $params[1];
        $pubId = $form->getData('currentPublicationId');
        $requestVars = $request->getUserVars();
        $subjectTaxonomy = $requestVars['subjectTaxonomy'];
        if (!$subjectTaxonomy) {
            $subjectTaxonomy = null;
        }

        // update the data
        $subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
        $subjectTaxonomyDao->updatePublicationSettings($pubId, $subjectTaxonomy);

        return false;
    }

    /**
     * Get Subject Taxonomy field value from the form
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataReadUserVars($hookName, $params)
    {
        $request = PKPApplication::get()->getRequest();
        // get the current context
        $context = $request->getContext();
        // get the template
        $templateMgr = TemplateManager::getManager($request);
        // get the form values
        $form = $params[0];
        // add t$userVars[] = 'competingInterests';he new field values to the user variables
        $userVars =& $params[1];
        $userVars[] = 'subjectTaxonomy';

        // Get all subjects
        $subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
        $subjects = $subjectTaxonomyDao->getByContextId($context->getId());
        $options = [0 => ''];
        while ($subjectTaxonomy = $subjects->next()) {
            $subjectTaxonomyId = $subjectTaxonomy->getId();
            $options[$subjectTaxonomyId] = $subjectTaxonomy->getLocalizedTitle();
        }

        $requestVars = $request->getUserVars();
        $subjectTaxonomy = $requestVars['subjectTaxonomy'];
        // add the current data to the template
        $templateVars = array('options' => $options, 'value' => $subjectTaxonomy);
        // get the template
        $templateMgr->assign($templateVars);

        return false;
    }


    /**
     * Set subject taxonomy value
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataExecute($hookName, $params) {
        // submitting the form with the new Subject Taxonomy value
        // get the form values
        $form =& $params[0];
        $publication = $form->submission->getCurrentPublication();

        $pubId = $publication->getData('id');

        // get the current Subject Taxonomy value
        $subjectTaxonomy = $form->getData('subjectTaxonomy');

        // update the data
        $subjectTaxonomyDao = DAORegistry::getDAO('SubjectTaxonomyDAO');
        $subjectTaxonomyDao->updatePublicationSettings($pubId, $subjectTaxonomy);
    }

}
