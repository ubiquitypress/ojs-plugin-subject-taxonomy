{**
 * templates/editSubjectTaxonomyForm.tpl
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * Form for editing a static page
 *}
<script src="{$pluginJavaScriptURL}/SubjectTaxonomyFormHandler.js"></script>
<script>
	$(function() {ldelim}
		// Attach the form handler.
		$('#subjectTaxonomyForm').pkpHandler(
			'$.pkp.controllers.form.subjectTaxonomy.SubjectTaxonomyFormHandler',
		);
	{rdelim});
</script>

{capture assign=actionUrl}{url router=$smarty.const.ROUTE_COMPONENT component="plugins.generic.subjectTaxonomy.controllers.grid.SubjectTaxonomyGridHandler" op="updateSubjectTaxonomy" escape=false}{/capture}
<form class="pkp_form" id="subjectTaxonomyForm" method="post" action="{$actionUrl}">
	{csrf}
	{if $subjectTaxonomyId}
		<input type="hidden" name="subjectTaxonomyId" value="{$subjectTaxonomyId|escape}" />
	{/if}
	{fbvFormArea id="subjectTaxonomyFormArea" class="border"}
		{fbvFormSection title="plugins.generic.subjectTaxonomy.title" for="title" required="true"}
			{fbvElement type="text" id="title" value=$title maxlength="255" multilingual=true }
		{/fbvFormSection}
	{/fbvFormArea}
	{fbvFormSection class="formButtons"}
		{assign var=buttonId value="submitFormButton"|concat:"-"|uniqid}
		{fbvElement type="submit" class="submitFormButton" id=$buttonId label="common.save"}
	{/fbvFormSection}
</form>
