{**
 * plugins/generic/subjectTaxonomy/subjectTaxonomy.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Edit SubjectTaxonomy subjectTaxonomy
 *
 *}

{if $options|@count gt 1}
	{fbvFormSection title="plugins.generic.subjectTaxonomy.subjectTaxonomy" for="subjectTaxonomy" required=false}
		{fbvElement type="select" name="subjectTaxonomy" id="subjectTaxonomy" from=$options selected=$value translate=false }
	{/fbvFormSection}
{/if}