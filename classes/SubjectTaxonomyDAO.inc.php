<?php

/**
 * @file classes/SubjectTaxonomyDAO.inc.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @package plugins.generic.subjectTaxonomy
 * @class SubjectTaxonomyDAO
 * Operations for retrieving and modifying SubjectTaxonomy objects.
 */

import('lib.pkp.classes.db.DAO');
import('plugins.generic.subjectTaxonomy.classes.SubjectTaxonomy');

class SubjectTaxonomyDAO extends DAO {

	/**
	 * Handle a cache miss.
	 * @param $cache GenericCache
	 * @param $id string
	 * @return Subject
	 */
	function _cacheMiss($cache, $id) {
		if ($cache->getCacheId() === 'current') {
			$subjectTaxonomy = $this->getCurrent($id, false);
		} else {
			$subjectTaxonomy = $this->getByBestId($id, null, false);
		}
		$cache->setCache($id, $subjectTaxonomy);
		return $subjectTaxonomy;
	}

	/**
	 * Get an subject cache by cache ID.
	 * @param $cacheId string
	 * @return GenericCache
	 */
	function _getCache($cacheId) {
		if (!isset($this->caches)) $this->caches = array();
		if (!isset($this->caches[$cacheId])) {
			$cacheManager = CacheManager::getManager();
			$this->caches[$cacheId] = $cacheManager->getObjectCache('subject_taxonomy', $cacheId, array($this, '_cacheMiss'));
		}
		return $this->caches[$cacheId];
	}

	/**
	 * Get a subject by ID
	 * @param $subjectTaxonomyId int Static page ID
	 * @param $contextId int Optional context ID
	 */
	function getById($subjectTaxonomyId, $contextId = null) {
		if ($useCache) {
			$cache = $this->_getCache('subjectTaxonomy');
			$returner = $cache->get($subjectTaxonomyId);
			if ($returner && $contextId != null && $contextId != $returner->getJournalId()) $returner = null;
			return $returner;
		}

		$params = array((int) $subjectTaxonomyId);
		if ($contextId) $params[] = $contextId;

		$result = $this->retrieve(
			'SELECT * FROM subject_taxonomy WHERE subject_taxonomy_id = ?'
			. ($contextId?' AND context_id = ?':''),
			$params
		);

		$row = $result->current();
		return $row ? $this->_fromRow((array) $row) : null;
	}

	/**
	 * Get a set of Subject Taxonmoy by context ID
	 * @param $contextId int
	 * @param $rangeInfo Object optional
	 * @return DAOResultFactory
	 */
	function getByContextId($contextId, $rangeInfo = null) {
		$result = $this->retrieveRange(
			'SELECT * FROM subject_taxonomy WHERE context_id = ? ORDER BY seq',
			[(int) $contextId],
			$rangeInfo
		);

		return new DAOResultFactory($result, $this, '_fromRow');
	}


	/**
	 * Insert a subject.
	 * @param $subjectTaxonomy SubjectTaxonomy
	 * @return int Inserted static page ID
	 */
	function insertObject($subjectTaxonomy) {
		$contextId= $subjectTaxonomy->getContextId();
		$this->update(
			'INSERT INTO subject_taxonomy (context_id, seq) VALUES (?, ?)',
			array(
				(int) $contextId,
				(float) $subjectTaxonomy->getSequence(),
			)
		);

		$subjectTaxonomy->setId($this->getInsertId());
		$this->updateLocaleFields($subjectTaxonomy);
		$this->resequenceSubjectTaxonomy($contextId);
		return $subjectTaxonomy->getId();
	}

	/**
	 * Update the database with a subject object
	 * @param $subjectTaxonomy SubjectTaxonomy
	 */
	function updateObject($subjectTaxonomy) {
		$contextId = $subjectTaxonomy->getContextId();
		$this->update(
			'UPDATE	subject_taxonomy
			SET	context_id = ?,
				seq = ? 
			WHERE	subject_taxonomy_id = ?',
			array(
				(int) $contextId,
				(float) $subjectTaxonomy->getSequence(),
				(int) $subjectTaxonomy->getId()
			)
		);
		$this->updateLocaleFields($subjectTaxonomy);
	}

	/**
	 * Delete a subject by ID.
	 * @param $subjectTaxonomyId int
	 */
	function deleteById($subjectTaxonomy) {
		$contextId = $subjectTaxonomy->getContextId();
		$subjectTaxonomyId = $subjectTaxonomy->getId();

		$this->update(
			'DELETE FROM subject_taxonomy WHERE subject_taxonomy_id = ?',
			[(int) $subjectTaxonomyId]
		);
		$this->update(
			'DELETE FROM subject_taxonomy_settings WHERE subject_taxonomy_id = ?',
			[(int) $subjectTaxonomyId]
		);
		$this->update(
			'DELETE FROM publication_settings WHERE setting_name = ? AND setting_value = ?',
			['subjectTaxonomy',(int) $subjectTaxonomyId]
		);
		$this->resequenceSubjectTaxonomy($contextId);		
	}

	/**
	 * Delete a subject object.
	 * @param $subjectTaxonomy SubjectTaxonomy
	 */
	function deleteObject($subjectTaxonomy) {
		$this->deleteById($subjectTaxonomy);
	}

	/**
	 * Generate a new subject object.
	 * @return SubjectTaxonomy
	 */
	function newDataObject() {
		return new SubjectTaxonomy();
	}

	/**
	 * Return a new Subject Taxonmoy object from a given row.
	 * @return SubjectTaxonomy
	 */
	function _fromRow($row) {
		$subjectTaxonomy = $this->newDataObject();
		$subjectTaxonomy->setId($row['subject_taxonomy_id']);
		$subjectTaxonomy->setContextId($row['context_id']);

		$subjectTaxonomy->setSequence($row['seq']);
		$this->getDataObjectSettings('subject_taxonomy_settings', 'subject_taxonomy_id', $row['subject_taxonomy_id'], $subjectTaxonomy);
		return $subjectTaxonomy;
	}

	/**
	 * Get the insert ID for the last inserted subject.
	 * @return int
	 */
	function getInsertId() {
		return $this->_getInsertId('subject_taxonomy', 'subject_taxonomy_id');
	}

	/**
	 * Get field names for which data is localized.
	 * @return array
	 */
	function getLocaleFieldNames() {
		return array('title');
	}

	/**
	 * Update the localized data for this object
	 * @param $author object
	 */
	function updateLocaleFields(&$subjectTaxonomy) {
		$this->updateDataObjectSettings('subject_taxonomy_settings', $subjectTaxonomy, array(
			'subject_taxonomy_id' => $subjectTaxonomy->getId()
		));
	}

	/**
	 * Sequentially renumber subjects in their sequence order.
	 * @param $journalId int Context ID
	 */
	function resequenceSubjectTaxonomy($contextId) {

		$result = $this->retrieve(
			'SELECT subject_taxonomy_id FROM subject_taxonomy WHERE context_id = ? ORDER BY seq',
			[(int) $contextId]
		);

		for ($i=1; $row = $result->current(); $i++) {
			$this->replace(
				'subject_taxonomy',
				[
					'subject_taxonomy_id' => $row->subject_taxonomy_id,
					'context_id' => (int) $contextId,
					'seq' => $i
				],
				['subject_taxonomy_id']
			);
			$result->next();
		}
	}

	/**
	 * replace an existing Subject Taxonomy record.
	 * @param $pubId integer
	 * @param $subjectTaxonomy array
	 */
	function updatePublicationSettings($pubId,$subjectTaxonomy) {
        $updateArr = [
        			  'publication_id' => $pubId,
                      'setting_name' => 'subjectTaxonomy',
                      'setting_value'  => $subjectTaxonomy
                     ];
        
        $keys = ['publication_id', 'locale', 'setting_name'];

		$result = $this->replace('publication_settings',$updateArr, $keys);
		return $result;
	}

}

