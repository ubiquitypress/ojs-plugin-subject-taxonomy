<?php

/**
 * @file classes/SubjectTaxonomy.inc.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @package plugins.generic.subjectTaxonomy
 * @class SubjectTaxonomy
 * Data object representing a static page.
 */

class SubjectTaxonomy extends DataObject {

	//
	// Get/set methods
	//

	/**
	 * Get subject ID
	 * @return int
	 */
	function getId(){
		return $this->getData('id');
	}

	/**
	 * Set subject ID
	 * @param $slideId int
	 */
	function setId($subjectTaxonomyId) {
		return $this->setData('id', $subjectTaxonomyId);
	}

	/**
	 * Get context ID
	 * @return string
	 */
	function getContextId(){
		return $this->getData('contextId');
	}

	/**
	 * Set context ID
	 * @param $contextId int
	 */
	function setContextId($contextId) {
		return $this->setData('contextId', $contextId);
	}


	/**
	 * Set subject title
	 * @param string string
	 * @param locale
	 */
	function setTitle($title, $locale) {
		return $this->setData('title', $title, $locale);
	}

	/**
	 * Get subject title
	 * @param locale
	 * @return string
	 */
	function getTitle($locale) {
		return $this->getData('title', $locale);
	}

	/**
	 * Get Localized subject title
	 * @return string
	 */
	function getLocalizedTitle() {
		return $this->getLocalizedData('title');
	}

		/**
	 * Get sequence of subject.
	 * @return float
	 */
	function getSequence() {
		return $this->getData('seq');
	}

	/**
	 * Set sequence of subject.
	 * @param $sequence float
	 */
	function setSequence($sequence) {
		$this->setData('seq', $sequence);
	}

}

