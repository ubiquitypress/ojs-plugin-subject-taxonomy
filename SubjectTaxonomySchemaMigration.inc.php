<?php

/**
 * @file classes/migration/SubjectTaxonomySchemaMigration.inc.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2000-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @class SubjectTaxonomySchemaMigration
 * @brief Describe database table structures.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Builder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;

class SubjectTaxonomySchemaMigration extends Migration {
        /**
         * Run the migrations.
         * @return void
         */
        public function up() {
		// List of subjects for each context
		Capsule::schema()->create('subject_taxonomy', function (Blueprint $table) {
			$table->bigInteger('subject_taxonomy_id')->autoIncrement();
			$table->float('seq', 8, 2)->default(0);
			$table->bigInteger('context_id');
		});

		// Subject Taxonomy settings.
		Capsule::schema()->create('subject_taxonomy_settings', function (Blueprint $table) {
			$table->bigInteger('subject_taxonomy_id');
			$table->string('locale', 14)->default('');
			$table->string('setting_name', 255);
			$table->longText('setting_value')->nullable();
			$table->string('setting_type', 6)->comment('(bool|int|float|string|object)');
			$table->index(['subject_taxonomy_id'], 'subject_taxonomy_settings_subject_taxonomy_id');
			$table->unique(['subject_taxonomy_id', 'locale', 'setting_name'], 'subject_taxonomy_settings_pkey');
		});

	}
}