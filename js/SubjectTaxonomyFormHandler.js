/**
 * @file js/SubjectTaxonomyFormHandler.js
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2000-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @package plugins.generic.subjectTaxonomy
 * @class SubjectTaxonomyFormHandler
 *
 * @brief Subject Taxonmoy form handler.
 */
(function($) {

	/** @type {Object} */
	$.pkp.controllers.form.subjectTaxonomy =
			$.pkp.controllers.form.subjectTaxonomy || { };



	/**
	 * @constructor
	 *
	 * @extends $.pkp.controllers.form.AjaxFormHandler
	 *
	 * @param {jQueryObject} $formElement A wrapped HTML element that
	 *  represents the approved proof form interface element.
	 * @param {Object} options Tabbed modal options.
	 */
	$.pkp.controllers.form.subjectTaxonomy.SubjectTaxonomyFormHandler =
			function($formElement, options) {
		this.parent($formElement, options);

		// Save the preview URL for later
		this.previewUrl_ = options.previewUrl;

		// bind a handler to make sure we update the required state
		// of the comments field.
		$('#previewButton', $formElement).click(this.callbackWrapper(
				this.showPreview_));
	};
	$.pkp.classes.Helper.inherits(
			$.pkp.controllers.form.subjectTaxonomy.SubjectTaxonomyFormHandler,
			$.pkp.controllers.form.AjaxFormHandler
	);

/** @param {jQuery} $ jQuery closure. */
}(jQuery));
